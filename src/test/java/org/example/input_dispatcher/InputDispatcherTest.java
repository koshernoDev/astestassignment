package org.example.input_dispatcher;

import org.example.input_dispatcher.operation_handler.IOperationHandler;
import org.example.input_dispatcher.operation_handler.OperationHandlerFactory;
import org.example.input_dispatcher.criteria_handler.CriteriaHandlerFactory;
import org.example.input_dispatcher.criteria_handler.ICriteriaHandler;
import org.junit.Assert;
import org.junit.Test;

public class InputDispatcherTest {

    @Test
    public void operationHandlerFactoryShouldBuildSearchOperationHandler() {

        IOperationHandler operationHandler = OperationHandlerFactory.getInstance("search");

        try {
            Assert.assertEquals(operationHandler.getClass(), Class.forName("org.example.input_dispatcher.operation_handler.SearchOperationHandler"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void operationHandlerFactoryShouldBuildStatOperationHandler() {

        IOperationHandler operationHandler = OperationHandlerFactory.getInstance("stat");

        try {
            Assert.assertEquals(operationHandler.getClass(), Class.forName("org.example.input_dispatcher.operation_handler.StatOperationHandler"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void operationHandlerFactoryShouldNotBuildOperationHandler() {

        IOperationHandler operationHandler = OperationHandlerFactory.getInstance(null);

        Assert.assertNull(operationHandler);
    }

    @Test
    public void criteriaHandlerFactoryShouldBuildLastNameCriteriaHandler() {

        ICriteriaHandler criteriaHandler = CriteriaHandlerFactory.getInstance("lastName");

        try {
            Assert.assertEquals(criteriaHandler.getClass(), Class.forName("org.example.input_dispatcher.criteria_handler.LastNameCriteria"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void criteriaHandlerFactoryShouldBuildProductNameAndTimesCriteriaHandler() {

        ICriteriaHandler criteriaHandler = CriteriaHandlerFactory.getInstance("minTimes");

        try {
            Assert.assertEquals(criteriaHandler.getClass(), Class.forName("org.example.input_dispatcher.criteria_handler.ProductNameAndTimesCriteria"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void criteriaHandlerFactoryShouldBuildMinMaxExpensesCriteriaHandler() {

        ICriteriaHandler criteriaHandler = CriteriaHandlerFactory.getInstance("minExpenses");

        try {
            Assert.assertEquals(criteriaHandler.getClass(), Class.forName("org.example.input_dispatcher.criteria_handler.MinMaxExpensesCriteria"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void criteriaHandlerFactoryShouldBuildBadCustomersCriteriaHandler() {

        ICriteriaHandler criteriaHandler = CriteriaHandlerFactory.getInstance("badCustomers");

        try {
            Assert.assertEquals(criteriaHandler.getClass(), Class.forName("org.example.input_dispatcher.criteria_handler.BadCustomersCriteria"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void criteriaHandlerFactoryShouldNotBuildCriteriaHandler() {

        ICriteriaHandler criteriaHandler = CriteriaHandlerFactory.getInstance(null);

        Assert.assertEquals(criteriaHandler, null);
    }
}
