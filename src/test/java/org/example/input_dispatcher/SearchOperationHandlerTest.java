package org.example.input_dispatcher;

import org.example.file_worker.FileWorker;
import org.example.input_dispatcher.operation_handler.IOperationHandler;
import org.example.input_dispatcher.operation_handler.SearchOperationHandler;
import org.example.json_example.JsonSimple;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class SearchOperationHandlerTest {

    @Test
    public void shouldCompleteSearchForOnlyLastNameCriteria() {

        JSONObject customer1 = new JSONObject();
        customer1.put("lastName", "Иванов");
        customer1.put("firstName", "Антон");

        JSONObject customer2 = new JSONObject();
        customer2.put("lastName", "Иванов");
        customer2.put("firstName", "Николай");

        JSONArray customers = new JSONArray();
        customers.add(customer1);
        customers.add(customer2);

        JSONObject requestCriteria = new JSONObject();
        requestCriteria.put("lastName", "Иванов");

        JSONObject responseItem = new JSONObject();
        responseItem.put("criteria", requestCriteria);
        responseItem.put("results", customers);

        JSONArray responseItems = new JSONArray();
        responseItems.add(responseItem);

        JSONObject expectedObject = new JSONObject();
        expectedObject.put("type", "search");
        expectedObject.put("results", responseItems);

        String filename = "src/test/resources/search_input_2.json";
        String filedata = FileWorker.readFile(filename);
        JSONObject request = JsonSimple.stringToJson(filedata);

        IOperationHandler handler = new SearchOperationHandler();
        JSONObject actualObject = handler.handle(request);

//        printObjects(expectedObject, actualObject);

        Assert.assertEquals(expectedObject.toJSONString(), actualObject.toJSONString());
    }


    @Test
    public void shouldCompleteSearchForOnlyProductNameAndTimesCriteria() {

        JSONObject customer1 = new JSONObject();
        customer1.put("lastName", "Кузнецов");
        customer1.put("firstName", "Алексей");

        JSONObject customer2 = new JSONObject();
        customer2.put("lastName", "Смирнов");
        customer2.put("firstName", "Николай");

        JSONArray customers = new JSONArray();
        customers.add(customer1);
        customers.add(customer2);

        JSONObject requestCriteria = new JSONObject();
        requestCriteria.put("minTimes", 3L);
        requestCriteria.put("productName", "Печенье шоколадное");

        JSONObject responseItem = new JSONObject();
        responseItem.put("criteria", requestCriteria);
        responseItem.put("results", customers);

        JSONArray responseItems = new JSONArray();
        responseItems.add(responseItem);

        JSONObject expectedObject = new JSONObject();
        expectedObject.put("type", "search");
        expectedObject.put("results", responseItems);

        String filename = "src/test/resources/search_input_3.json";
        String filedata = FileWorker.readFile(filename);
        JSONObject request = JsonSimple.stringToJson(filedata);

        IOperationHandler handler = new SearchOperationHandler();
        JSONObject actualObject = handler.handle(request);

//        printObjects(expectedObject, actualObject);

        Assert.assertEquals(expectedObject.toJSONString(), actualObject.toJSONString());
    }

    @Test
    public void shouldCompleteSearchForOnlyMinMaxExpensesCriteria() {

        JSONObject customer1 = new JSONObject();
        customer1.put("lastName", "Петров");
        customer1.put("firstName", "Валентин");

        JSONObject customer2 = new JSONObject();
        customer2.put("lastName", "Иванов");
        customer2.put("firstName", "Антон");

        JSONArray customers = new JSONArray();
        customers.add(customer1);
        customers.add(customer2);

        JSONObject requestCriteria = new JSONObject();
        requestCriteria.put("minExpenses", 100L);
        requestCriteria.put("maxExpenses", 300L);

        JSONObject responseItem = new JSONObject();
        responseItem.put("criteria", requestCriteria);
        responseItem.put("results", customers);

        JSONArray responseItems = new JSONArray();
        responseItems.add(responseItem);

        JSONObject expectedObject = new JSONObject();
        expectedObject.put("type", "search");
        expectedObject.put("results", responseItems);

        String filename = "src/test/resources/search_input_4.json";
        String filedata = FileWorker.readFile(filename);
        JSONObject request = JsonSimple.stringToJson(filedata);

        IOperationHandler handler = new SearchOperationHandler();
        JSONObject actualObject = handler.handle(request);

//        printObjects(expectedObject, actualObject);

        Assert.assertEquals(expectedObject.toJSONString(), actualObject.toJSONString());
    }

    @Test
    public void shouldCompleteSearchForOnlyBadCustomersCriteria() {

        JSONObject customer1 = new JSONObject();
        customer1.put("lastName", "Петров");
        customer1.put("firstName", "Андрей");

        JSONObject customer2 = new JSONObject();
        customer2.put("lastName", "Петров");
        customer2.put("firstName", "Валентин");

        JSONObject customer3 = new JSONObject();
        customer3.put("lastName", "Иванов");
        customer3.put("firstName", "Николай");

        JSONArray customers = new JSONArray();
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);

        JSONObject requestCriteria = new JSONObject();
        requestCriteria.put("badCustomers", 3L);

        JSONObject responseItem = new JSONObject();
        responseItem.put("criteria", requestCriteria);
        responseItem.put("results", customers);

        JSONArray responseItems = new JSONArray();
        responseItems.add(responseItem);

        JSONObject expectedObject = new JSONObject();
        expectedObject.put("type", "search");
        expectedObject.put("results", responseItems);

        String filename = "src/test/resources/search_input_5.json";
        String filedata = FileWorker.readFile(filename);
        JSONObject request = JsonSimple.stringToJson(filedata);

        IOperationHandler handler = new SearchOperationHandler();
        JSONObject actualObject = handler.handle(request);

//        printObjects(expectedObject, actualObject);

        Assert.assertEquals(expectedObject.toJSONString(), actualObject.toJSONString());
    }

    @Test
    public void shouldCompleteSearchForEveryCriterias() {

        JSONObject customer11 = new JSONObject();
        customer11.put("lastName", "Иванов");
        customer11.put("firstName", "Антон");

        JSONObject customer12 = new JSONObject();
        customer12.put("lastName", "Иванов");
        customer12.put("firstName", "Николай");

        JSONArray customers1 = new JSONArray();
        customers1.add(customer11);
        customers1.add(customer12);

        JSONObject requestCriteria1 = new JSONObject();
        requestCriteria1.put("lastName", "Иванов");

        JSONObject responseItem1 = new JSONObject();
        responseItem1.put("criteria", requestCriteria1);
        responseItem1.put("results", customers1);



        JSONObject customer21 = new JSONObject();
        customer21.put("lastName", "Кузнецов");
        customer21.put("firstName", "Алексей");

        JSONObject customer22 = new JSONObject();
        customer22.put("lastName", "Смирнов");
        customer22.put("firstName", "Николай");

        JSONArray customers2 = new JSONArray();
        customers2.add(customer21);
        customers2.add(customer22);

        JSONObject requestCriteria2 = new JSONObject();
        requestCriteria2.put("minTimes", 3L);
        requestCriteria2.put("productName", "Печенье шоколадное");

        JSONObject responseItem2 = new JSONObject();
        responseItem2.put("criteria", requestCriteria2);
        responseItem2.put("results", customers2);



        JSONObject customer31 = new JSONObject();
        customer31.put("lastName", "Петров");
        customer31.put("firstName", "Валентин");

        JSONObject customer32 = new JSONObject();
        customer32.put("lastName", "Иванов");
        customer32.put("firstName", "Антон");

        JSONArray customers3 = new JSONArray();
        customers3.add(customer31);
        customers3.add(customer32);

        JSONObject requestCriteria3 = new JSONObject();
        requestCriteria3.put("minExpenses", 100L);
        requestCriteria3.put("maxExpenses", 300L);

        JSONObject responseItem3 = new JSONObject();
        responseItem3.put("criteria", requestCriteria3);
        responseItem3.put("results", customers3);



        JSONObject customer41 = new JSONObject();
        customer41.put("lastName", "Петров");
        customer41.put("firstName", "Андрей");

        JSONObject customer42 = new JSONObject();
        customer42.put("lastName", "Петров");
        customer42.put("firstName", "Валентин");

        JSONObject customer43 = new JSONObject();
        customer43.put("lastName", "Иванов");
        customer43.put("firstName", "Николай");

        JSONArray customers4 = new JSONArray();
        customers4.add(customer41);
        customers4.add(customer42);
        customers4.add(customer43);

        JSONObject requestCriteria4 = new JSONObject();
        requestCriteria4.put("badCustomers", 3L);

        JSONObject responseItem4 = new JSONObject();
        responseItem4.put("criteria", requestCriteria4);
        responseItem4.put("results", customers4);



        JSONArray responseItems = new JSONArray();
        responseItems.add(responseItem1);
        responseItems.add(responseItem2);
        responseItems.add(responseItem3);
        responseItems.add(responseItem4);

        JSONObject expectedObject = new JSONObject();
        expectedObject.put("type", "search");
        expectedObject.put("results", responseItems);

        String filename = "src/test/resources/search_input_1.json";
        String filedata = FileWorker.readFile(filename);
        JSONObject request = JsonSimple.stringToJson(filedata);

        IOperationHandler handler = new SearchOperationHandler();
        JSONObject actualObject = handler.handle(request);

//        printObjects(expectedObject, actualObject);

        Assert.assertEquals(expectedObject.toJSONString(), actualObject.toJSONString());
    }

    private void printObjects(JSONObject expectedObject, JSONObject actualObject) {
        System.out.println("expectedObject.toJSONString() = " + expectedObject.toJSONString());
        System.out.println("actualObject.toJSONString() = " + actualObject.toJSONString());
    }
}