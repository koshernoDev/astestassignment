package org.example.input_dispatcher;

import org.example.input_dispatcher.criteria_handler.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class CriteriaHandlerTest {

    @Test
    public void lastNameHandlerShouldFetchCorrectDataFromDB() {

        JSONObject request = new JSONObject();
        request.put("lastName", "Иванов");

        JSONObject customer1 = new JSONObject();
        customer1.put("lastName", "Иванов");
        customer1.put("firstName", "Антон");

        JSONObject customer2 = new JSONObject();
        customer2.put("lastName", "Иванов");
        customer2.put("firstName", "Николай");

        JSONArray customers = new JSONArray();
        customers.add(customer1);
        customers.add(customer2);

        JSONObject expectedObject = new JSONObject();
        expectedObject.put("criteria", request);
        expectedObject.put("results", customers);

        ICriteriaHandler handler = new LastNameCriteria();
        JSONObject actualObject = handler.handle(request);

//        printObjects(expectedObject, actualObject);

        Assert.assertEquals(expectedObject.toJSONString(), actualObject.toJSONString());
    }

    @Test
    public void productNameAndTimesHandlerShouldFetchCorrectDataFromDB() {

        JSONObject request = new JSONObject();
        request.put("productName", "Печенье шоколадное");
        request.put("minTimes", 3L);

        JSONObject customer1 = new JSONObject();
        customer1.put("lastName", "Кузнецов");
        customer1.put("firstName", "Алексей");

        JSONObject customer2 = new JSONObject();
        customer2.put("lastName", "Смирнов");
        customer2.put("firstName", "Николай");

        JSONArray customers = new JSONArray();
        customers.add(customer1);
        customers.add(customer2);

        JSONObject expectedObject = new JSONObject();
        expectedObject.put("criteria", request);
        expectedObject.put("results", customers);

        ICriteriaHandler handler = new ProductNameAndTimesCriteria();
        JSONObject actualObject = handler.handle(request);

//        printObjects(expectedObject, actualObject);

        Assert.assertEquals(expectedObject.toJSONString(), actualObject.toJSONString());
    }

    @Test
    public void minMaxExpensesHandlerShouldFetchCorrectDataFromDB() {

        JSONObject request = new JSONObject();
        request.put("minExpenses", 100L);
        request.put("maxExpenses", 300L);

        JSONObject customer1 = new JSONObject();
        customer1.put("lastName", "Петров");
        customer1.put("firstName", "Валентин");

        JSONObject customer2 = new JSONObject();
        customer2.put("lastName", "Иванов");
        customer2.put("firstName", "Антон");

        JSONArray customers = new JSONArray();
        customers.add(customer1);
        customers.add(customer2);

        JSONObject expectedObject = new JSONObject();
        expectedObject.put("criteria", request);
        expectedObject.put("results", customers);

        ICriteriaHandler handler = new MinMaxExpensesCriteria();
        JSONObject actualObject = handler.handle(request);

//        printObjects(expectedObject, actualObject);

        Assert.assertEquals(expectedObject.toJSONString(), actualObject.toJSONString());
    }

    @Test
    public void badCustomersHandlerShouldFetchCorrectDataFromDB() {

        JSONObject request = new JSONObject();
        request.put("badCustomers", 3L);

        JSONObject customer1 = new JSONObject();
        customer1.put("lastName", "Петров");
        customer1.put("firstName", "Андрей");

        JSONObject customer2 = new JSONObject();
        customer2.put("lastName", "Петров");
        customer2.put("firstName", "Валентин");

        JSONObject customer3 = new JSONObject();
        customer3.put("lastName", "Иванов");
        customer3.put("firstName", "Николай");

        JSONArray customers = new JSONArray();
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);

        JSONObject expectedObject = new JSONObject();
        expectedObject.put("criteria", request);
        expectedObject.put("results", customers);

        ICriteriaHandler handler = new BadCustomersCriteria();
        JSONObject actualObject = handler.handle(request);

//        printObjects(expectedObject, actualObject);

        Assert.assertEquals(expectedObject.toJSONString(), actualObject.toJSONString());
    }

    private void printObjects(JSONObject expectedObject, JSONObject actualObject) {
        System.out.println("expectedObject.toJSONString() = " + expectedObject.toJSONString());
        System.out.println("actualObject.toJSONString() = " + actualObject.toJSONString());
    }
}
