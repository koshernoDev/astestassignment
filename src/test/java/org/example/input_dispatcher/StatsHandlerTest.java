package org.example.input_dispatcher;

import org.example.file_worker.FileWorker;
import org.example.input_dispatcher.criteria_handler.ICriteriaHandler;
import org.example.input_dispatcher.criteria_handler.TimeIntervalStats;
import org.example.input_dispatcher.operation_handler.IOperationHandler;
import org.example.input_dispatcher.operation_handler.OperationHandlerFactory;
import org.example.json_example.JsonSimple;
import org.json.simple.JSONObject;
import org.junit.Test;

public class StatsHandlerTest {

    @Test
    public void foo() {
        String filename = "src/test/resources/stat_input_1.json";
        String filedata = FileWorker.readFile(filename);
        JSONObject request = JsonSimple.stringToJson(filedata);

        ICriteriaHandler handler = new TimeIntervalStats();
        handler.handle(request);
    }

    @Test
    public void bar() {
        String filename = "src/test/resources/stat_input_1.json";
        String filedata = FileWorker.readFile(filename);
        JSONObject request = JsonSimple.stringToJson(filedata);

        IOperationHandler handler = OperationHandlerFactory.getInstance("stat");

        JSONObject handledRequest = handler.handle(request);

        System.out.println("handledRequest.toJSONString() = " + handledRequest.toJSONString());
    }
}
