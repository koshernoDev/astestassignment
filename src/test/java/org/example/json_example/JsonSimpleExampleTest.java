package org.example.json_example;

import org.example.file_worker.FileWorker;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JsonSimpleExampleTest {

    @Test
    public void shouldReadSearchInputOneJsonFileAndToJsonIt() {

        JSONObject lastNameCriteria = new JSONObject();
        lastNameCriteria.put("lastName", "Иванов");

        JSONObject nameTimesCriteria = new JSONObject();
        nameTimesCriteria.put("productName", "Печенье шоколадное");
        nameTimesCriteria.put("minTimes", 3);

        JSONObject minMaxExpensesCriteria = new JSONObject();
        minMaxExpensesCriteria.put("minExpenses", 100);
        minMaxExpensesCriteria.put("maxExpenses", 300);

        JSONObject badCriteria = new JSONObject();
        badCriteria.put("badCustomers", 3);

        JSONArray criteriaArray = new JSONArray();
        criteriaArray.add(lastNameCriteria);
        criteriaArray.add(nameTimesCriteria);
        criteriaArray.add(minMaxExpensesCriteria);
        criteriaArray.add(badCriteria);

        JSONObject inputJson = new JSONObject();
        inputJson.put("criterias", criteriaArray);

        String filename = "src/test/resources/search_input_1.json";
        String filedata = FileWorker.readFile(filename);
        JSONObject actualJson = JsonSimple.stringToJson(filedata);

        assertEquals(inputJson.toJSONString(), actualJson.toJSONString());
    }

    @Test
    public void shouldReadStatInputOneJsonFileAndToJsonIt() {

        JSONObject stat = new JSONObject();
        stat.put("startDate", "2021-03-11");
        stat.put("endDate", "2021-03-22");

        String filename = "src/test/resources/stat_input_1.json";
        String filedata = FileWorker.readFile(filename);
        JSONObject actualJson = JsonSimple.stringToJson(filedata);

        assertEquals(stat.toJSONString(), actualJson.toJSONString());
    }
}
