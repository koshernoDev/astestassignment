package org.example.postgresql;

import org.junit.Assert;
import org.junit.Test;

public class SqlQueryTest {

    @Test
    public void builderShouldConstructCorrectQueryFromGivenStatements() {

        SqlQuery query = new SqlQuery.Builder()
                .addStatement("SELECT * FROM table\n")
                .addStatement("WHERE field = 1\n")
                .addStatement("LIMIT 3")
                .build();

        String expectedQuery = "SELECT * FROM table\n" +
                "WHERE field = 1\n" +
                "LIMIT 3";

        Assert.assertEquals(expectedQuery, query.getQuery());
    }
}
