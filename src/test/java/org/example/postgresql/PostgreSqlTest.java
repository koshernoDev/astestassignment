package org.example.postgresql;

import org.junit.Assert;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PostgreSqlTest {

    private class Customer {

        private final String firstName;
        private final String lastName;

        Customer(String firstName, String lastName) {

            this.firstName = firstName;
            this.lastName = lastName;
        }

        @Override
        public boolean equals(Object o) {

            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Customer customer = (Customer) o;

            return firstName.equals(customer.firstName) && lastName.equals(customer.lastName);
        }

        @Override
        public String toString() {

            return """
                    Customer{
                        firstName='$firstName', 
                        lastName='$lastName'
                    }""";
        }
    }

    @Test
    public void shouldGetFirstFiveCustomersResultSetFromCustomersTable() {

        Customer[] customers = new Customer[]{
                new Customer("Антон", "Иванов"),
                new Customer("Николай", "Иванов"),
                new Customer("Валентин", "Петров"),
                new Customer("Андрей", "Петров"),
                new Customer("Николай", "Смирнов")
        };
        SqlQuery query = new SqlQuery.Builder()
                .addStatement("SELECT * FROM public.customers\n")
                .addStatement("ORDER BY id\n")
                .addStatement("LIMIT 5")
                .build();


        PostgreSql example = new PostgreSql();
        example.setQuery(query.getQuery());
        ResultSet resultSet = example.runQuery();

        Customer[] actualCustomers = new Customer[customers.length];
        try {
            int i = 0;
            while (resultSet.next()) {
                String fName = resultSet.getString("firstname");
                String lName = resultSet.getString("lastname");
                actualCustomers[i] = new Customer(fName, lName);
                ++i;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        Assert.assertArrayEquals(customers, actualCustomers);
    }

    @Test
    public void shouldGetAllCustomersWithGivenLastNameFromCustomersTable() {

        Customer[] customers = new Customer[]{
                new Customer("Валентин", "Петров"),
                new Customer("Андрей", "Петров")
        };
        SqlQuery query = new SqlQuery.Builder()
                .addStatement("SELECT * FROM public.customers\n")
                .addStatement("WHERE lastname = 'Петров'\n")
                .addStatement("ORDER BY id ASC\n")
                .addStatement("LIMIT 2\n")
                .build();

        PostgreSql example = new PostgreSql();
        example.setQuery(query.getQuery());
        ResultSet resultSet = example.runQuery();

        Customer[] actualCustomers = new Customer[customers.length];
        try {
            int i = 0;
            while (resultSet.next()) {
                String fName = resultSet.getString("firstname");
                String lName = resultSet.getString("lastname");
                actualCustomers[i] = new Customer(fName, lName);
                ++i;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        Assert.assertArrayEquals(customers, actualCustomers);
    }

    @Test
    public void shouldGetAllCustomersThatBoughtGivenGoodMoreThanGivenTimes() {

        Customer[] customers = new Customer[]{
                new Customer("Алексей", "Кузнецов"),
                new Customer("Николай", "Смирнов")
        };
        SqlQuery query = new SqlQuery.Builder()
                .addStatement("SELECT lastname, firstname, name, COUNT(*)\n")
                .addStatement("FROM purchases \n")
                .addStatement("JOIN customers ON purchases.customer_id = customers.id\n")
                .addStatement("JOIN goods ON purchases.good_id = goods.id \n")
                .addStatement("GROUP BY name, lastname, firstname\n")
                .addStatement("HAVING COUNT(*) > 3 AND name = 'Печенье шоколадное'")
                .build();

        PostgreSql example = new PostgreSql();
        example.setQuery(query.getQuery());
        ResultSet resultSet = example.runQuery();

        Customer[] actualCustomers = new Customer[customers.length];
        try {
            int i = 0;
            while (resultSet.next()) {
                String fName = resultSet.getString("firstname");
                String lName = resultSet.getString("lastname");
                actualCustomers[i] = new Customer(fName, lName);
                ++i;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        Assert.assertArrayEquals(customers, actualCustomers);
    }

    @Test
    public void shouldGetAllCustomersWithTotalExpensesInGivenInterval() {

        Customer[] customers = new Customer[]{
                new Customer("Валентин", "Петров"),
                new Customer("Антон", "Иванов")
        };
        SqlQuery query = new SqlQuery.Builder()
                .addStatement("SELECT lastname, firstname, SUM(price)\n")
                .addStatement("FROM purchases\n")
                .addStatement("JOIN goods ON purchases.good_id = goods.id\n")
                .addStatement("JOIN customers ON purchases.customer_id = customers.id\n")
                .addStatement("GROUP BY customers.id\n")
                .addStatement("HAVING SUM(price) BETWEEN 100 AND 300")
                .build();

        PostgreSql example = new PostgreSql();
        example.setQuery(query.getQuery());
        ResultSet resultSet = example.runQuery();

        Customer[] actualCustomers = new Customer[customers.length];
        try {
            int i = 0;
            while (resultSet.next()) {
                String fName = resultSet.getString("firstname");
                String lName = resultSet.getString("lastname");
                actualCustomers[i] = new Customer(fName, lName);
                ++i;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        Assert.assertArrayEquals(customers, actualCustomers);
    }

    @Test
    public void shouldGetBadCustomersCounterWithLimit() {

        Customer[] customers = new Customer[]{
                new Customer("Андрей", "Петров"),
                new Customer("Валентин", "Петров"),
                new Customer("Николай", "Иванов")
        };
        SqlQuery query = new SqlQuery.Builder()
                .addStatement("SELECT lastname, firstname, COUNT(*)\n")
                .addStatement("FROM purchases\n")
                .addStatement("JOIN customers ON purchases.customer_id = customers.id\n")
                .addStatement("JOIN goods ON purchases.customer_id = goods.id\n")
                .addStatement("GROUP BY customers.id\n")
                .addStatement("ORDER BY COUNT(*)\n")
                .addStatement("LIMIT 3")
                .build();

        PostgreSql example = new PostgreSql();
        example.setQuery(query.getQuery());
        ResultSet resultSet = example.runQuery();

        Customer[] actualCustomers = new Customer[customers.length];
        try {
            int i = 0;
            while (resultSet.next()) {
                String fName = resultSet.getString("firstname");
                String lName = resultSet.getString("lastname");
                actualCustomers[i] = new Customer(fName, lName);
                ++i;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        Assert.assertArrayEquals(customers, actualCustomers);
    }
}
