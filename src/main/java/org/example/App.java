package org.example;

import org.example.file_worker.FileWorker;
import org.example.input_dispatcher.operation_handler.IOperationHandler;
import org.example.input_dispatcher.operation_handler.OperationHandlerFactory;
import org.example.json_example.JsonSimple;
import org.json.simple.JSONObject;

public class App {

    public static void main(String[] args) {

        if (args.length != 3) return;

        for (String arg : args) {
            System.out.println("arg = " + arg);
        }

        String operation = args[0];
        String inputFile = args[1];
        String outputFile = args[2];

        String filedata = FileWorker.readFile(inputFile);
        JSONObject request = JsonSimple.stringToJson(filedata);
        IOperationHandler operationHandler = OperationHandlerFactory.getInstance(operation);

        JSONObject handledOperation = operationHandler.handle(request);

        FileWorker.formatJsonAndWriteToFile(outputFile, handledOperation);
    }
}
