package org.example.postgresql;

import java.sql.*;

public class PostgreSql {

    private String url = "jdbc:postgresql://localhost:5432/example";
    private String user = "postgres";
    private String password = "user";

    public void setQuery(String query) {
        this.query = query;
    }

    private String query = "SELECT * FROM public.customers";

    public ResultSet runQuery() {

        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            Class.forName("org.postgresql.Driver");

            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(query);

            return resultSet;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
