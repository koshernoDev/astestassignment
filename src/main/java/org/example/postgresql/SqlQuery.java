package org.example.postgresql;

import java.util.ArrayList;

public class SqlQuery {

    public String getQuery() {
        return query;
    }

    private String query;

    private SqlQuery(Builder builder) {
        this.query = builder.statements.stream()
                .reduce((acc, st) -> acc + st)
                .get();
    }

    public static class Builder {

        private ArrayList<String> statements = new ArrayList<>();

        public Builder addStatement(String statement) {

            statements.add(statement);

            return this;
        }

        public SqlQuery build() {
            return new SqlQuery(this);
        }
    }
}
