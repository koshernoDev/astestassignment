package org.example.json_example;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonSimple {

    public static JSONObject stringToJson(String string) {

        JSONParser jsonParser = new JSONParser();

        try {
            JSONObject object = (JSONObject) jsonParser.parse(string);

            return object;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String jsonToString(JSONObject object) {
        return object.toJSONString();
    }
}
