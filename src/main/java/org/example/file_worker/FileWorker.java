package org.example.file_worker;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileWorker {

    static public String readFile(String filename) {

        try {
            String content = Files.readString(Path.of(filename), StandardCharsets.UTF_8);

            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    static public void formatJsonAndWriteToFile(String filename, JSONObject json) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        try {
            mapper.writeValue(new File(filename), json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
