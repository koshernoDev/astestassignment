package org.example.input_dispatcher.operation_handler;

import org.example.input_dispatcher.criteria_handler.CriteriaHandlerFactory;
import org.example.input_dispatcher.criteria_handler.ICriteriaHandler;
import org.json.simple.JSONObject;

public class StatOperationHandler implements IOperationHandler{

    static {
        OperationHandlerFactory.register("stat", new StatOperationHandler());
    }

    @Override
    public JSONObject handle(JSONObject json) {

        System.out.println("Stats");

        String criteriaIdentifier = (String) json.keySet().iterator().next();
        ICriteriaHandler handler = CriteriaHandlerFactory.getInstance(criteriaIdentifier);

        JSONObject handledRequest = handler.handle(json);

        return handledRequest;
    }
}
