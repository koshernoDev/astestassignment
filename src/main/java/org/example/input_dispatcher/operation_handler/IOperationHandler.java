package org.example.input_dispatcher.operation_handler;

import org.json.simple.JSONObject;

public interface IOperationHandler {

    JSONObject handle(JSONObject json);
}
