package org.example.input_dispatcher.operation_handler;

import org.apache.commons.io.IOUtils;

import java.net.URL;
import java.net.URLConnection;
import java.util.*;

public class OperationHandlerFactory {

    private static final Map<String, IOperationHandler> instances = new HashMap<>();

    static {
        try {
            String path = "org/example/input_dispatcher/operation_handler";
            loadClasses(OperationHandlerFactory.class.getClassLoader(), path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void register(String operation, IOperationHandler instance) {
        if (operation != null && instance != null) {
            instances.put(operation, instance);
        }
    }

    public static IOperationHandler getInstance(String operation) {

        if (instances.containsKey(operation)) {
            return instances.get(operation);
        }

        return null;
    }

    private static void loadClasses(ClassLoader cl, String packagePath) throws Exception {

        String dottedPackage = packagePath.replaceAll("[/]", ".");

        URL upackage = cl.getResource(packagePath);
        URLConnection conn = upackage.openConnection();

        String rr = IOUtils.toString(conn.getInputStream(), "UTF-8");

        if (rr != null) {
            String[] paths = rr.split("\n");

            for (String p : paths) {
                if (p.endsWith(".class")) {
                    Class.forName(dottedPackage + "." + p.substring(0, p.lastIndexOf('.')));
                }
            }
        }
    }
}
