package org.example.input_dispatcher.operation_handler;

import org.example.input_dispatcher.criteria_handler.CriteriaHandlerFactory;
import org.example.input_dispatcher.criteria_handler.ICriteriaHandler;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class SearchOperationHandler implements IOperationHandler {

    static {
        OperationHandlerFactory.register("search", new SearchOperationHandler());
    }

    @Override
    public JSONObject handle(JSONObject json) {

        System.out.println("Searching");

        Map<JSONObject, ICriteriaHandler> criteriaToHandlers = new LinkedHashMap<>();

        JSONArray criteriasToHandle = (JSONArray) json.get("criterias");

        for (Object obj : criteriasToHandle) {
            if (obj instanceof JSONObject) {
                String criteriaIdentifier = (String) ((JSONObject) obj).keySet().iterator().next();
                ICriteriaHandler handler = CriteriaHandlerFactory.getInstance(criteriaIdentifier);
                criteriaToHandlers.put((JSONObject) obj, handler);
            }
        }

        JSONObject handledOperation = new JSONObject();
        handledOperation.put("type", "search");

        JSONArray responses = new JSONArray();

        for (Iterator it = criteriaToHandlers.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry criteriaAndHandler = (Map.Entry) it.next();
            JSONObject criteria = (JSONObject) criteriaAndHandler.getKey();
            ICriteriaHandler handler = (ICriteriaHandler) criteriaAndHandler.getValue();

            JSONObject response = handler.handle(criteria);

            responses.add(response);

            it.remove();
        }

        handledOperation.put("results", responses);

        return handledOperation;
    }
}
