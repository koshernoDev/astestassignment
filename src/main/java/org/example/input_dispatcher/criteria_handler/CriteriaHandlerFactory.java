package org.example.input_dispatcher.criteria_handler;

import org.apache.commons.io.IOUtils;
import org.example.input_dispatcher.operation_handler.OperationHandlerFactory;

import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

public class CriteriaHandlerFactory {

    private static final Map<String, ICriteriaHandler> instances = new HashMap<>();

    static {
        try {
            String path = "org/example/input_dispatcher/criteria_handler";
            loadClasses(OperationHandlerFactory.class.getClassLoader(), path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void register(String criteria, ICriteriaHandler instance) {
        if (criteria != null && instance != null) {
            instances.put(criteria, instance);
        }
    }

    public static ICriteriaHandler getInstance(String criteria) {

        if (instances.containsKey(criteria)) {
            return instances.get(criteria);
        }

        return null;
    }

    private static void loadClasses(ClassLoader cl, String packagePath) throws Exception {

        String dottedPackage = packagePath.replaceAll("[/]", ".");

        URL upackage = cl.getResource(packagePath);
        URLConnection conn = upackage.openConnection();

        String rr = IOUtils.toString(conn.getInputStream(), "UTF-8");

        if (rr != null) {
            String[] paths = rr.split("\n");

            for (String p : paths) {
                if (p.endsWith(".class")) {
                    Class.forName(dottedPackage + "." + p.substring(0, p.lastIndexOf('.')));
                }
            }
        }
    }
}
