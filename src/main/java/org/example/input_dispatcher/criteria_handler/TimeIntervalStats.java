package org.example.input_dispatcher.criteria_handler;

import org.example.json_example.JsonSimple;
import org.example.postgresql.PostgreSql;
import org.example.postgresql.SqlQuery;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class TimeIntervalStats implements ICriteriaHandler {

    static {
        CriteriaHandlerFactory.register("endDate", new TimeIntervalStats());
    }

    private Map<String, List<JSONObject>> customerExpenses = new LinkedHashMap<>();

    public synchronized void addToList(String mapKey, JSONObject myItem) {
        List<JSONObject> itemsList = customerExpenses.get(mapKey);

        // if list does not exist create it
        if(itemsList == null) {
            itemsList = new ArrayList<JSONObject>();
            itemsList.add(myItem);
            customerExpenses.put(mapKey, itemsList);
        } else {
            // add if item is not already in list
            if(!itemsList.contains(myItem)) itemsList.add(myItem);
        }
    }

    @Override
    public JSONObject handle(JSONObject json) {

        System.out.println("Handling stats");

        String startDate = (String) json.get("startDate");
        String endDate = (String) json.get("endDate");

        SqlQuery query = new SqlQuery.Builder()
                .addStatement("WITH interval_expenses (customer_id, lastname, firstname, good_name, good_expenses)\n")
                .addStatement("AS (SELECT customers.id, customers.lastname, customers.firstname, goods.name, goods.price\n")
                .addStatement("FROM purchases\n")
                .addStatement("JOIN customers ON purchases.customer_id = customers.id\n")
                .addStatement("JOIN goods ON purchases.good_id = goods.id\n")
                .addStatement("WHERE purchases.date BETWEEN '" + startDate + "' AND '" + endDate + "'\n")
                .addStatement("ORDER BY customers.id)\n")
                .addStatement("SELECT lastname, firstname, good_name, SUM(good_expenses)\n")
                .addStatement("FROM interval_expenses\n")
                .addStatement("GROUP BY customer_id, lastname, firstname, good_name\n")
                .addStatement("ORDER BY customer_id, SUM(good_expenses) DESC")
                .build();

        PostgreSql postgreSql = new PostgreSql();
        postgreSql.setQuery(query.getQuery());
        ResultSet resultSet = postgreSql.runQuery();

        formCustomersExpensesList(resultSet);

        JSONArray customersData = new JSONArray();
        Long totalExpenses = 0L;

        for (Iterator it = customerExpenses.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry criteriaAndHandler = (Map.Entry) it.next();
            String customer = (String) criteriaAndHandler.getKey();
            ArrayList<JSONObject> purchases = (ArrayList<JSONObject>) criteriaAndHandler.getValue();

            JSONArray customerPurchases = new JSONArray();


            Long customerTotalExpenses = 0L;

            for (JSONObject purchase : purchases) {
                customerPurchases.add(purchase);
                customerTotalExpenses += (Long) purchase.get("expenses");
            }

            totalExpenses += customerTotalExpenses;

            JSONObject customerData = new JSONObject();
            customerData.put("name", customer);
            customerData.put("purchases", customerPurchases);
            customerData.put("totalExpenses", customerTotalExpenses);

            customersData.add(customerData);

            it.remove();
        }

        Long totalDays = getTotalDays(startDate, endDate);

        JSONObject handledOperation = getUnorderedHandledOperationJson(customersData, totalExpenses, totalDays);

        // TODO do its JSON to be unordered.
        return handledOperation;
    }

    private void formCustomersExpensesList(ResultSet resultSet) {

        try {
            while (resultSet.next()) {
                String firstname = resultSet.getString("firstname");
                String lastName = resultSet.getString("lastname");
                String name = lastName + " " + firstname;

                String goodName = resultSet.getString("good_name");
                Long sum = resultSet.getLong("sum");

                JSONObject goodExpenses = new JSONObject();
                goodExpenses.put("name", goodName);
                goodExpenses.put("expenses", sum);

                addToList(name, goodExpenses);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private JSONObject getUnorderedHandledOperationJson(JSONArray customersData, Long totalExpenses, Long totalDays) {

        Map temp = new LinkedHashMap();
        temp.put("type", "stat");
        temp.put("totalDays", totalDays);
        temp.put("customers", customersData);
        temp.put("totalExpenses", totalExpenses);
        temp.put("avgExpenses", totalExpenses.doubleValue() / customersData.size());

//        String handledOperation = JSONObject.toJSONString(temp);
        JSONObject handledOperation = new JSONObject(temp);

        return handledOperation;
    }

    private Long getTotalDays(String startDate, String endDate) {

        Long totalDays = 0L;

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
            Date firstDate = sdf.parse(startDate);
            Date secondDate = sdf.parse(endDate);

            long diffInMillis = Math.abs(secondDate.getTime() - firstDate.getTime());
            totalDays = TimeUnit.DAYS.convert(diffInMillis, TimeUnit.MILLISECONDS);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        return totalDays;
    }
}