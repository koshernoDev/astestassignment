package org.example.input_dispatcher.criteria_handler;

import org.example.postgresql.SqlQuery;
import org.json.simple.JSONObject;

public class ProductNameAndTimesCriteria implements ICriteriaHandler {

    static {
        CriteriaHandlerFactory.register("minTimes", new ProductNameAndTimesCriteria());
    }

    @Override
    public JSONObject handle(JSONObject json) {

        System.out.println("Handling product name and times");

        String productName = (String) json.get("productName");
        Long times = (Long) json.get("minTimes");

        SqlQuery query = new SqlQuery.Builder()
                .addStatement("SELECT lastname, firstname, name, COUNT(*)\n")
                .addStatement("FROM purchases\n")
                .addStatement("JOIN customers ON purchases.customer_id = customers.id\n")
                .addStatement("JOIN goods ON purchases.good_id = goods.id\n")
                .addStatement("GROUP BY name, lastname, firstname\n")
                .addStatement("HAVING COUNT(*) > " + times + " AND name = '" + productName + "'")
                .build();

        return DBFetcher.getJsonObject(json, query);
    }
}
