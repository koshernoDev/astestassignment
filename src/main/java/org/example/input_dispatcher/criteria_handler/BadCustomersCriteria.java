package org.example.input_dispatcher.criteria_handler;

import org.example.postgresql.SqlQuery;
import org.json.simple.JSONObject;

public class BadCustomersCriteria implements ICriteriaHandler {

    static {
        CriteriaHandlerFactory.register("badCustomers", new BadCustomersCriteria());
    }

    @Override
    public JSONObject handle(JSONObject json) {

        System.out.println("Handling bad customers");

        Long badCustomers = (Long) json.get("badCustomers");

        SqlQuery query = new SqlQuery.Builder()
                .addStatement("SELECT lastname, firstname, COUNT(*)\n")
                .addStatement("FROM purchases\n")
                .addStatement("JOIN customers ON purchases.customer_id = customers.id\n")
                .addStatement("JOIN goods ON purchases.customer_id = goods.id\n")
                .addStatement("GROUP BY customers.id\n")
                .addStatement("ORDER BY COUNT(*)\n")
                .addStatement("LIMIT " + badCustomers)
                .build();

        return DBFetcher.getJsonObject(json, query);
    }
}
