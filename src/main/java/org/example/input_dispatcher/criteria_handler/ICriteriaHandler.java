package org.example.input_dispatcher.criteria_handler;

import org.json.simple.JSONObject;

public interface ICriteriaHandler {

    JSONObject handle(JSONObject json);
}
