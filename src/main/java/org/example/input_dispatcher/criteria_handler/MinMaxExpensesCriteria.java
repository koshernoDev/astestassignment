package org.example.input_dispatcher.criteria_handler;

import org.example.postgresql.SqlQuery;
import org.json.simple.JSONObject;

public class MinMaxExpensesCriteria implements ICriteriaHandler {

    static {
        CriteriaHandlerFactory.register("minExpenses", new MinMaxExpensesCriteria());
    }

    @Override
    public JSONObject handle(JSONObject json) {

        System.out.println("Handling min max expenses");

        Long minExpenses = (Long) json.get("minExpenses");
        Long maxExpenses = (Long) json.get("maxExpenses");

        SqlQuery query = new SqlQuery.Builder()
                .addStatement("SELECT lastname, firstname, SUM(price)\n")
                .addStatement("FROM purchases\n")
                .addStatement("JOIN goods ON purchases.good_id = goods.id\n")
                .addStatement("JOIN customers ON purchases.customer_id = customers.id\n")
                .addStatement("GROUP BY customers.id\n")
                .addStatement("HAVING SUM(price) BETWEEN " + minExpenses + " AND " + maxExpenses)
                .build();

        return DBFetcher.getJsonObject(json, query);
    }
}
