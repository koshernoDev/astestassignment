package org.example.input_dispatcher.criteria_handler;

import org.example.postgresql.PostgreSql;
import org.example.postgresql.SqlQuery;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DBFetcher {

    public static JSONObject getJsonObject(JSONObject json, SqlQuery query) {

        PostgreSql postgreSql = new PostgreSql();
        postgreSql.setQuery(query.getQuery());
        ResultSet resultSet = postgreSql.runQuery();

        JSONObject criteriaResult = new JSONObject();

        criteriaResult.put("criteria", json);

        JSONArray customers = new JSONArray();

        try {
            while (resultSet.next()) {
                String name = resultSet.getString("firstname");
                String lastName = resultSet.getString("lastname");

                JSONObject customer = new JSONObject();
                customer.put("lastName", lastName);
                customer.put("firstName", name);

                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        criteriaResult.put("results", customers);

        return criteriaResult;
    }
}
