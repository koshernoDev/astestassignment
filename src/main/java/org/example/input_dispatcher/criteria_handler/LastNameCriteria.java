package org.example.input_dispatcher.criteria_handler;

import org.example.postgresql.SqlQuery;
import org.json.simple.JSONObject;


public class LastNameCriteria implements  ICriteriaHandler {

    static {
        CriteriaHandlerFactory.register("lastName", new LastNameCriteria());
    }

    @Override
    public JSONObject handle(JSONObject json) {

        System.out.println("Handling lastname");

        String lastname = (String) json.get("lastName");

        SqlQuery query = new SqlQuery.Builder()
                .addStatement("SELECT * FROM public.customers\n")
                .addStatement("WHERE lastname = '" + lastname + "'\n")
                .addStatement("ORDER BY id ASC\n")
                .build();

        return DBFetcher.getJsonObject(json, query);
    }
}
